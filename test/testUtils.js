import checkPropTypes from "check-prop-types";
import { createStore } from "redux";

import { reducer } from "../src/store";

/**
 * Create a testing store with importe reducers, middleware and initial state.
 * @function storeFactory
 * @param {object} initialState - Initial state for store
 * @returns {Store}
 */
export const storeFactory = (initialState) => {
  return createStore(reducer, initialState);
};

/**
 * Return node(s) with the given data-test attribute.
 * @param {ShallowWrapper} wrapper - Enzyme shallow wrapper.
 * @param {string} value - Value of data-test attribute for search.
 * @return {ShallowWrapper}
 */
export const findByTestAttr = (wrapper, value) => {
  return wrapper.find(`[data-test="${value}"]`);
};
    
export const checkProps = (component, conformingProps) => {
  const propError = checkPropTypes(
    component.propTypes,
    conformingProps,
    'prop',
    component.name
  );

  expect(propError).toBeUndefined();
};
