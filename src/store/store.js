import React from "react";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import thunk from "redux-thunk";

import reducer from "./reducer";

const store = createStore(reducer, applyMiddleware(thunk));

const StoreProvider = ({ children }) => <Provider store={store}>{children}</Provider>;

const { dispatch } = store;

export { StoreProvider, store, dispatch };
