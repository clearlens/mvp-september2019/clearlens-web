import { connect } from "react-redux";

const mapThemeToProps = ComponentToConnect => {
  const mapStateToProps = state => {
    return {
      theme: state.theme,
    };
  };

  return connect(mapStateToProps)(ComponentToConnect);
};

export default mapThemeToProps;
