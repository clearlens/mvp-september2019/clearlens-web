export const themes = {
  DEFAULT: "dark",
  DARK: "dark",
  LIGHT: "light",
};

// User Settings related state
export const actionTypes = {
  SWITCH_THEME: "SWITCH_THEME",
};

export const setTheme = theme => {
  return { type: actionTypes.SWITCH_THEME, theme };
};
