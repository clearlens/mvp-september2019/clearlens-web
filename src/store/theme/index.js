export { default as themeReducer } from "./themeReducer";
export * from "./themeActions";

export { default as mapThemeToProps } from "./mapThemeToProps";
