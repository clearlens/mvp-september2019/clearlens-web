import { actionTypes, themes } from "./themeActions";

let sessionStorageState;
try {
  sessionStorageState = JSON.parse(sessionStorage.getItem("theme"));
} catch (reason) {
  // do nothing
}

const initialState = sessionStorageState || themes.DEFAULT;

const themeReducer = (state = initialState, action) => {
  let resultState = state;

  if (action.type === actionTypes.SWITCH_THEME) {
    resultState = action.theme;
  }

  sessionStorage.setItem("theme", JSON.stringify(resultState));
  return resultState;
};

export default themeReducer;
