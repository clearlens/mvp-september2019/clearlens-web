import { actionTypes } from "./authActions";

const initialState = { authenticated: false, tokens: null, useSSO: true };

const authReducer = (state = initialState, action) => {
  let resultState = state;

  if (action.type === actionTypes.AUTHENTICATED) {
    if (state.authenticated) return state;

    resultState = {
      ...state,
      authenticated: action.authInfo.authenticated,
      tokens: action.authInfo.tokens,
    };

    if (action.callback) action.callback();
  } else if (
    action.type === actionTypes.AUTH_FAILED ||
    action.type === actionTypes.LOGGEDOUT ||
    action.type === actionTypes.LOGOUT_FAILED
  ) {
    resultState = {
      ...state,
      authenticated: false,
      tokens: null,
    };
  }

  return resultState;
};

export default authReducer;
