// Auth related state
export const actionTypes = {
  AUTHENTICATED: "AUTHENTICATED",
  AUTH_FAILED: "AUTH_FAILED",
  LOGGEDOUT: "LOGGEDOUT",
  LOGOUT_FAILED: "LOGOUT_FAILED",
};

export const authenticated = (authInfo, callback) => {
  return { type: actionTypes.AUTHENTICATED, authInfo, callback };
};

export const authenticationFailed = errorInfo => {
  return { type: actionTypes.AUTH_FAILED, errorInfo };
};

export const loggedOut = () => {
  return { type: actionTypes.LOGGEDOUT };
};

export const logoutFailed = errorInfo => {
  return { type: actionTypes.LOGOUT_FAILED, errorInfo };
};
