import { connect } from "react-redux";

const mapAuthToProps = ComponentToConnect => {
  const mapStateToProps = state => {
    return {
      auth: state.auth,
    };
  };

  return connect(
    mapStateToProps,
    null
  )(ComponentToConnect);
};

export default mapAuthToProps;
