export default {
  routes: [
    {
      path: "/dashboard",
      title: "Dashboard",
      default: true,
      headerNav: true,
      routes: [
        {
          path: "/overview",
          title: "Overview",
          default: true,
        },
        {
          path: "/my-team",
          title: "My Team",
        },
        {
          path: "/all-teams",
          title: "All Teams",
        },
        {
          path: "/behaviour-trends",
          title: "Behaviour Trends",
        },
      ],
    },
    {
      path: "/cases",
      title: "Cases",
      headerNav: true,
      routes: [
        {
          path: "/my-cases",
          title: "My Cases",
          default: true,
          components: [
            {
              name: "case.CaseBoard",
            },
          ],
        },
        {
          path: "/my-teams-cases",
          title: "My Team's Cases",
          components: [
            {
              name: "case.CaseBoard",
            },
          ],
        },
        {
          path: "/all-cases",
          title: "All Cases",
          components: [
            {
              name: "case.CaseList",
            },
          ],
        },
      ],
    },
    {
      path: "/case/:caseId",
      routes: [
        {
          path: "/review",
          title: "Review",
          icon: "bullseye",
          default: true,
          components: [
            {
              name: "case.RelatedActors",
            },
            {
              name: "case.Narrative",
            },
            {
              name: "case.Alerts",
            },
          ],
        },
        {
          path: "/research",
          title: "Research",
          icon: "dna",
        },
        {
          path: "/evidence",
          title: "Evidence",
          icon: "library",
        },
        {
          path: "/narrate",
          title: "Narrate",
          icon: "scroll",
        },
        {
          path: "/summarize",
          title: "Summarize",
          icon: "clipboard",
        },
      ],
    },
  ],
};
