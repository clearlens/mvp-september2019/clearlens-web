import React from "react";
import ReactDOM from "react-dom";
import { HashRouter as Router } from "react-router-dom";

import "normalize.css/normalize.css";
import "@blueprintjs/core/lib/css/blueprint.css";
import "@blueprintjs/icons/lib/css/blueprint-icons.css";

import App from "./app/App";
import * as serviceWorker from "./serviceWorker";
import { StoreProvider } from "./store";
import { CaseContextProvider } from "./contexts/CaseContext";

import "./index.scss";
import { AuthProvider } from "./app/utils/AuthProvider";

const app = (
  <StoreProvider>
    <AuthProvider>
      <CaseContextProvider>
        <Router>
          <App />
        </Router>
      </CaseContextProvider>
    </AuthProvider>
  </StoreProvider>
);

ReactDOM.render(app, document.getElementById("root"));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
