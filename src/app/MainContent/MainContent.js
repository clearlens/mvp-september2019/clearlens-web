import React from "react";
import { Route } from "react-router-dom";

import { componentRegistry } from "../../components/utils";
import { mapThemeToProps } from "../../store/theme";
import SubNavigatorDock from "./SubNavigationDock/SubNavigationDock";
import Lucy from "../../lucy/Lucy";
import cssClasses from "./MainContent.module.scss";

function MainContent(props) {
  const { theme, routes, rootPath: rawRootPath } = props;

  const prepRootPathWithValues = originalRootPath => {
    let newRootPath = originalRootPath;

    Object.entries(props.match.params).forEach(entry => {
      newRootPath = newRootPath.replace(`:${entry[0]}`, entry[1]);
    });

    return newRootPath;
  };

  const getComponentFromRegistry = (componentInfo, routerProps) => {
    const { name } = componentInfo;
    const Component = componentRegistry[name];
    return Component ? < Component key={`content-${name}`} {...routerProps}/> : null;
  };

  const renderMainContent = (route, routerProps) => {
    const { components = [] } = route;

    return (
      <>
        {components.length > 0 &&
          components.map(component => getComponentFromRegistry(component, routerProps))}
      </>);
  };

  const rootPath = prepRootPathWithValues(rawRootPath);

  const contentScope = "case-review";

  return (
    <div className={cssClasses.root} data-theme={theme}>
      <div className={cssClasses.dockLeftContainer}>
        <SubNavigatorDock routes={routes} rootPath={rootPath} {...props} />
      </div>

      <div className={cssClasses.mainArea}>
        <div className={cssClasses.mainContainer}>
          {routes.map(route => (
            <Route
              path={rootPath + route.path}
              render={routeProps => renderMainContent(route, routeProps)}
              key={`content-route::${rootPath + route.path}`}
            />
          ))}
        </div>

        <footer className={cssClasses.footerContainer}>Powered by ClearLens</footer>
      </div>

      <div className={cssClasses.dockRightContainer}>
        <Lucy scope={contentScope} {...props} />
      </div>
    </div>
  );
}

export default mapThemeToProps(MainContent);
