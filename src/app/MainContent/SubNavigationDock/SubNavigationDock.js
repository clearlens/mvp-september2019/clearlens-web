import React, { useState } from "react";
import { Redirect, Route } from "react-router-dom";
import cssClasses from "./SubNavigationDock.module.scss";
import { ClarityIcon, SideDock, SideDockPosition } from "../../../components/utils";

function SubNavigatorDock({ routes, rootPath, theme }) {
  const [isExpanded, setIsExpanded] = useState(false);

  const toggle = expanded => {
    setIsExpanded(expanded);
  };

  let defaultPath;

  return (
    <SideDock position={SideDockPosition.LEFT} onToggle={toggle}>
      <div className={cssClasses.root} data-expanded={isExpanded} data-theme={theme}>
        {routes.map(route => {
          const routePath = rootPath + route.path;
          if (route.default) {
            defaultPath = routePath;
          }
          return (
            <Route key={routePath} path={routePath} exact>
              {routeProps => {
                const isSelected = routeProps.match && routeProps.match.path === routePath;
                const iconName = route.icon || "circle";
                return (
                  <a
                    href={`/#${routePath}`}
                    className={cssClasses.navItem}
                    data-selected={isSelected}>
                    <ClarityIcon className={cssClasses.navItemIcon} shape={iconName} />
                    {<span className={cssClasses.navItemLabel}> {route.title}</span>}
                  </a>
                );
              }}
            </Route>
          );
        })}
        {defaultPath && (
          <Route exact path={rootPath} render={() => <Redirect to={defaultPath} />} />
        )}
      </div>
    </SideDock>
  );
}

export default SubNavigatorDock;
