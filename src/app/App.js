import React from "react";
import { Redirect, Route } from "react-router-dom";
import classNames from "classnames";
import { Classes as bpClasses } from "@blueprintjs/core";

import { mapThemeToProps, themes } from "../store/theme";
import routerConfig from "../RouterConfig";

import MainContent from "./MainContent/MainContent";
import Header from "./Header/Header";

import cssClasses from "./App.module.scss";

function App(props) {
  const { theme } = props;

  const renderContent = (routerProps, route) => {
    return (
      <MainContent
        {...routerProps}
        rootPath={route.path}
        initPath={route.initPath}
        routes={route.routes}
      />
    );
  };

  let defaultPath;
  return (
    <div
      data-test="app"
      className={classNames(cssClasses.root, { [bpClasses.DARK]: theme === themes.DARK })}
      data-theme={theme}>
      <Header />
      <div>
        {routerConfig.routes.map(route => {
          if (route.default) {
            defaultPath = route.path;
          }

          return (
            <Route
              key={route.path}
              exact={route.exact === true}
              path={route.path}
              render={routerProps => renderContent(routerProps, route)}
            />
          );
        })}
        {defaultPath && <Route exact path="/" render={() => <Redirect to={defaultPath} />} />}
      </div>
    </div>
  );
}

export default mapThemeToProps(App);
