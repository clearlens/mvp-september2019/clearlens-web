import React from "react";
import Enzyme, { shallow } from "enzyme/build";
import EnzymeAdapter from "enzyme-adapter-react-16/build";
import App from "./App";
import { findByTestAttr, storeFactory } from "../../test/testUtils";
import { themes } from "../store/theme";

Enzyme.configure({ adapter: new EnzymeAdapter() });

const createWrapper = theme => {
  const store = storeFactory({ theme });

  return shallow(<App store={store} />)
    .dive()
    .dive();
};

describe("<App />", () => {
  test("renders without crashing", () => {
    const wrapper = createWrapper(themes.DEFAULT);
    const app = findByTestAttr(wrapper, "app");
    expect(app.length).toBe(1);
    expect(app.find("[data-theme='dark']").exists()).toBe(true);
  });

  test("renders correctly with dark theme set", () => {
    const wrapper = createWrapper(themes.DARK);
    const app = findByTestAttr(wrapper, "app");
    expect(app.length).toBe(1);
    expect(app.find("[data-theme='dark']").exists()).toBe(true);
  });

  test("renders correctly with light theme set", () => {
    const wrapper = createWrapper(themes.LIGHT);
    const app = findByTestAttr(wrapper, "app");
    expect(app.length).toBe(1);
    expect(app.find("[data-theme='light']").exists()).toBe(true);
  });
});
