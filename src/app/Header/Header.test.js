import React from "react";
import Enzyme, { shallow } from "enzyme";
import EnzymeAdapter from "enzyme-adapter-react-16";
import { storeFactory } from "../../../test/testUtils";
import { themes } from "../../store/theme/themeActions";
import Header from "./Header";

Enzyme.configure({ adapter: new EnzymeAdapter() });

const createHeader = theme => {
  const store = storeFactory({ theme });
  return shallow(<Header store={store} />);
};

describe("<Header />", () => {
  test("if it renders", () => {
    const header = createHeader(themes.DEFAULT);
    expect(header).toHaveLength(1);
  });
});
