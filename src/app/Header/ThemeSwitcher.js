/* eslint-disable jsx-a11y/label-has-for */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React from "react";
// import Switch from "react-switch";
import { Switch } from "@blueprintjs/core";
import { dispatch } from "../../store";
import { setTheme, themes, mapThemeToProps } from "../../store/theme";
import cssClasses from "./ThemeSwitcher.module.scss";

function ThemeSwitcherBase({ theme }) {
  const toggleTheme = () => {
    if (theme === themes.LIGHT) {
      dispatch(setTheme(themes.DARK));
    } else {
      dispatch(setTheme(themes.LIGHT));
    }
  };
  return (
    <div className={cssClasses.root}>
      <Switch
        alignIndicator="right"
        checked={theme === themes.DARK}
        label="Dark Theme"
        onChange={toggleTheme}
        className={cssClasses.reactSwitch}
        id="material-switch"
        inline="true"
      />
    </div>
  );
}
const ThemeSwitcher = mapThemeToProps(ThemeSwitcherBase);
export default ThemeSwitcher;
