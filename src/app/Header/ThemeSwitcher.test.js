import React from "react";
import Enzyme, { shallow } from "enzyme";
import EnzymeAdapter from "enzyme-adapter-react-16";
import { storeFactory } from "../../../test/testUtils";
import { themes } from "../../store/theme/themeActions";
import ThemeSwitcher from "./ThemeSwitcher";

Enzyme.configure({ adapter: new EnzymeAdapter() });

const createSwitcher = theme => {
  const store = storeFactory({ theme });
  return shallow(<ThemeSwitcher store={store} />);
};

describe("<ThemeSwitcher />", () => {
  test("if it renders", () => {
    const themeSwitcher = createSwitcher(themes.DEFAULT);
    expect(themeSwitcher).toHaveLength(1);
  });
});
