import React from "react";
import Enzyme, { shallow } from "enzyme";
import EnzymeAdapter from "enzyme-adapter-react-16";
import { storeFactory } from "../../../test/testUtils";
import { themes } from "../../store/theme/themeActions";
import User from "./User";

Enzyme.configure({ adapter: new EnzymeAdapter() });

const createUser = theme => {
  const store = storeFactory({ theme });
  return shallow(<User store={store} />);
};

describe("<User />", () => {
  test("if it renders", () => {
    const user = createUser(themes.DEFAULT);
    expect(user).toHaveLength(1);
  });
});
