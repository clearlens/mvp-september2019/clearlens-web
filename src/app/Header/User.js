import React from "react";

import { mapThemeToProps } from "../../store/theme";
import { logout } from "../utils/AuthProvider";
// import { ClarityIcon } from "../../components/utils";

import cssClasses from "./User.module.scss";

function HeaderBase({ theme }) {
  return (
    <div data-test="user-menu" className={cssClasses.root} data-theme={theme}>
      <button
        type="button"
        onClick={() => {
          logout();
        }}>
        <p className={cssClasses.userInitials}>GK</p>
      </button>
    </div>
  );
}

const Header = mapThemeToProps(HeaderBase);

export default Header;
