import React, { useState } from "react";

import ThemeSwitcher from "./ThemeSwitcher";
import LucinityLogo from "../../assest/images/LucinityLogo";
import { mapThemeToProps } from "../../store/theme";
import User from "./User";
import ApplicationsMenu from "./ApplicationsMenu";
import { ClarityIcon } from "../../components/utils";

import cssClasses from "./Header.module.scss";

/* When the openFullscreen() function is executed, open the video in fullscreen.
Note that we must include prefixes for different browsers, as they don't support the requestFullscreen method yet */
const openFullscreen = () => {
  /* Get the element you want displayed in fullscreen mode (a video in this example): */
  const elem = document.documentElement;
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.mozRequestFullScreen) {
    /* Firefox */
    elem.mozRequestFullScreen();
  } else if (elem.webkitRequestFullscreen) {
    /* Chrome, Safari and Opera */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) {
    /* IE/Edge */
    elem.msRequestFullscreen();
  }
};
/* Close fullscreen */
function closeFullscreen() {
  if (document.exitFullscreen) {
    document.exitFullscreen();
  } else if (document.mozCancelFullScreen) {
    /* Firefox */
    document.mozCancelFullScreen();
  } else if (document.webkitExitFullscreen) {
    /* Chrome, Safari and Opera */
    document.webkitExitFullscreen();
  } else if (document.msExitFullscreen) {
    /* IE/Edge */
    document.msExitFullscreen();
  }
}
function HeaderBase({ theme }) {
  const [isFullscreen, setIsFullscreen] = useState(false);
  const userActions = {
    search: "search",
    user: "myCases",
    users: "teamCases",
    keyboard: "hotKeys",
    resize: "fullscreen",
    shrink: "shrink",
  };
  const userFuncitons = action => {
    switch (action) {
      case "search":
        break;
      case "fullscreen":
        openFullscreen();
        setIsFullscreen(true);
        break;
      case "shrink":
        closeFullscreen();
        setIsFullscreen(false);
        break;
      default:
        break;
    }
  };

  return (
    <header data-test="app-header" className={cssClasses.root} data-theme={theme}>
      <div className={cssClasses.left}>
        <ApplicationsMenu theme={theme} />
        <div className={cssClasses.logo}>
          <LucinityLogo />
          <p>ClearLens</p>
        </div>
      </div>

      <div className={cssClasses.rightApplicationHeader}>
        {Object.keys(userActions).map(key => {
          if (isFullscreen && key === "resize") {
            return "";
          }
          if (!isFullscreen && key === "shrink") {
            return "";
          }
          return (
            <button key={key} type="button" onClick={() => userFuncitons(userActions[key])}>
              <ClarityIcon
                className={cssClasses.clarityIcon}
                shape={key}
                solid="true"
                size="2rem"
              />
            </button>
          );
        })}
        <ThemeSwitcher theme={theme} />
        <User />
      </div>
    </header>
  );
}

const Header = mapThemeToProps(HeaderBase);

export default Header;
