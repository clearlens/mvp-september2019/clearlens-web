import React from "react";
import Enzyme, { shallow } from "enzyme";
import EnzymeAdapter from "enzyme-adapter-react-16";
import { storeFactory } from "../../../test/testUtils";
import { themes } from "../../store/theme/themeActions";
import ApplicationsMenu from "./ApplicationsMenu";

Enzyme.configure({ adapter: new EnzymeAdapter() });

const createMenu = theme => {
  const store = storeFactory({ theme });
  return shallow(<ApplicationsMenu store={store} />);
};

describe("<ApplicationsMenu />", () => {
  test("if it renders", () => {
    const appMenu = createMenu(themes.DEFAULT);
    expect(appMenu).toHaveLength(1);
  });
});
