import React, { useState } from "react";
import classNames from "classnames";
import { ClarityIcon } from "../../components/utils";
import cssClasses from "./ApplicationMenu.module.scss";

function ApplicationsMenu({ theme }) {
  const [displayApplications, setDisplayApplications] = useState(false);
  return (
    <div className={classNames(cssClasses.root)} data-theme={theme}>
      <button
        type="button"
        className={classNames(
          cssClasses.modalOverlay,
          displayApplications ? "" : cssClasses.hidden
        )}
        onClick={() => {
          setDisplayApplications(!displayApplications);
        }}
      />
      <button
        type="button"
        className={classNames(cssClasses.application, displayApplications ? cssClasses.active : "")}
        onClick={() => {
          setDisplayApplications(!displayApplications);
        }}>
        <ClarityIcon shape="applications" solid="true" size="2.6rem" />
      </button>
      <ul
        className={classNames(
          cssClasses.appMenu,
          displayApplications ? cssClasses.applicationVisible : cssClasses.hidden
        )}>
        <li className={cssClasses.views}>
          <a href="#Dashboard">Dashboard</a>
        </li>
        <li className={cssClasses.views}>
          <a href="#Cases">Cases</a>
        </li>
        <ul className={cssClasses.recentCases}>
          Recent Cases
          <li>
            <a href="#Dashboard">C-1234-5678</a>
          </li>
          <li>
            <a href="#Cases">C-1234-5678</a>
          </li>
        </ul>
      </ul>
    </div>
  );
}
export default ApplicationsMenu;
