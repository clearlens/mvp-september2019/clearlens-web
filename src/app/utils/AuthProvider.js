import React from "react";
import Keycloak from "keycloak-js";
import { dispatch } from "../../store";
import { authenticated, authenticationFailed, loggedOut, mapAuthToProps } from "../../store/auth";

const keycloak = new Keycloak("./keycloak.json");

const useKeycloak = !!process.env.REACT_APP_USE_KEYCLOAK;

const refreshToken = () => {
  keycloak
    .updateToken(5)
    .success(refreshed => {
      if (refreshed) {
        console.debug("Token was successfully refreshed");
        dispatch(
          authenticated({
            authenticated: keycloak.authenticated,
            tokens: {
              token: keycloak.token,
            },
          })
        );
      } else {
        console.debug("Token is still valid");
      }
    })
    .error(error => {
      console.log("Failed to refresh the token", error);
      dispatch(authenticationFailed({ error }));
    });
};

const initKeycloak = () => {
  if (useKeycloak) {
    keycloak
      .init({ onLoad: "check-sso", checkLoginIframeInterval: 1 }) // promiseType: 'native'
      .success(result => {
        if (keycloak.authenticated) {
          console.log("authenticated ...", result);
          /* setTimeout(() => {
            dispatch(
              authenticated({
                authenticated: keycloak.authenticated,
                tokens: {
                  token: keycloak.token,
                },
              })
            );
          }, 1000);
*/
          setInterval(() => {
            refreshToken();
          }, 10000);
        } else {
          console.log("login ...");

          // setTimeout(() => {
          keycloak.login({ scope: "openid email profile address" });
          // }, 1000);
        }
      })
      .error(error => {
        console.log("error", error);
        dispatch(authenticationFailed({ error }));
      });
  }
};

initKeycloak();

const getHostname = () => {
  return window.location.href.replace(window.location.hash, "");
};

// Keycloak events
if (useKeycloak) {
  keycloak.onReady = param => {
    console.debug("XXX - onReady", param);
  };
  keycloak.onAuthSuccess = param => {
    console.debug("XXX - onAuthSuccess", param);

    dispatch(
      authenticated({
        authenticated: keycloak.authenticated,
        tokens: {
          token: keycloak.token,
        },
      })
    );
  };
  keycloak.onAuthError = param => {
    console.debug("XXX - onAuthError", param);
  };

  keycloak.onAuthRefreshSuccess = param => {
    console.debug("XXX - onAuthRefreshSuccess", param);
  };
  keycloak.onAuthRefreshError = param => {
    console.debug("XXX - onAuthRefreshError", param);
  };
  keycloak.onAuthLogout = param => {
    console.debug("XXX - onAuthLogout", param);

    dispatch(loggedOut());
  };
  keycloak.onTokenExpired = param => {
    console.debug("XXX - onTokenExpired", param);

    refreshToken();
  };
}

const AuthProviderBase = props => {
  const { auth, children } = props;

  if (!useKeycloak && !auth.authenticated) {
    console.log("Not using keycloak, authenticating user");
    dispatch(authenticated({ authenticated: true }));
  }

  return auth && auth.authenticated ? <>{children}</> : <div>Authenticating ...</div>;
};
export const AuthProvider = mapAuthToProps(AuthProviderBase);

export const logout = () => {
  keycloak.logout({ redirectUri: `${getHostname()}/logout.html` });
};
