/* eslint-disable react/no-danger */
import React from "react";
import classNames from "classnames";
import { AllShapes } from "@clr/icons/shapes/all-shapes";

import cssClasses from "./ClarityIcon.module.scss";

const BADGED_CLASS_SUBSTRING = "--badged";
const ALERTED_CLASS_SUBSTRING = "--alerted";
const SOLID_CLASS = "clr-i-solid";

export const ClarityIconDirection = {
  UP: "up",
  DOWN: "down",
  LEFT: "left",
  RIGHT: "right",
};

function ClarityIcon({
  shape,
  size = "1.6rem",
  solid = false,
  alert = false,
  badge = false,
  direction = "",
  className,
}) {
  if (!shape) {
    return <></>;
  }
  const content = AllShapes[shape];
  const canBadge = content.indexOf(BADGED_CLASS_SUBSTRING) > -1;
  const canAlert = content.indexOf(ALERTED_CLASS_SUBSTRING) > -1;
  const hasSolid = content.indexOf(SOLID_CLASS) > -1;

  const variantClasses = {
    [cssClasses.outline]: !(hasSolid && solid),
    [cssClasses.solid]: hasSolid && solid,
    [cssClasses.alert]: canAlert && alert,
    [cssClasses.badge]: canBadge && badge,
  };

  return (
    <span
      className={classNames(cssClasses.root, variantClasses, className)}
      style={{ width: size, height: size }}
      dangerouslySetInnerHTML={{ __html: content }}
      dir={direction}
    />
  );
}

export default ClarityIcon;
