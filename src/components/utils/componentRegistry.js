import { Alerts, CaseBoard, Narrative, RelatedActors, TeamStats } from "../case";

export default {
  "case.CaseBoard": CaseBoard,
  "case.Alerts": Alerts,
  "case.TeamStats": TeamStats,
  "case.RelatedActors": RelatedActors,
  "case.Narrative": Narrative,
};
