import React, { useState } from "react";
import PropTypes from "prop-types";

import cssClasses from "./SideDock.module.scss";
import { mapThemeToProps } from "../../../store/theme";
import ClarityIcon, { ClarityIconDirection } from "../ClarityIcon/ClarityIcon";

export const SideDockPosition = {
  LEFT: "left",
  RIGHT: "right",
};

function getTriggerDir(defaultTriggerDir, isExpanded) {
  if (isExpanded) {
    return defaultTriggerDir === ClarityIconDirection.LEFT
      ? ClarityIconDirection.RIGHT
      : ClarityIconDirection.LEFT;
  }

  return defaultTriggerDir;
}

function SideDock({
  children,
  theme,
  position = SideDockPosition.LEFT,
  expandWidth = "24rem",
  onToggle,
}) {
  const [isExpanded, setIsExpanded] = useState(false);
  const defaultTriggerDir =
    position === SideDockPosition.LEFT ? ClarityIconDirection.RIGHT : ClarityIconDirection.LEFT;
  const triggerDir = getTriggerDir(defaultTriggerDir, isExpanded);

  const toggle = () => {
    const expanded = !isExpanded;
    setIsExpanded(expanded);
    onToggle(expanded);
  };

  const inlineStyles = {
    width: isExpanded ? expandWidth : "4.5rem",
  };

  return (
    <nav
      className={cssClasses.root}
      data-theme={theme}
      data-pos={position}
      data-expanded={isExpanded}
      style={inlineStyles}>
      <button type="button" className={cssClasses.trigger} onClick={() => toggle()}>
        <ClarityIcon
          className={cssClasses.triggerIcon}
          shape="angle-double"
          direction={triggerDir}
        />
      </button>
      <div className={cssClasses.dockContent}>{children}</div>
    </nav>
  );
}

SideDock.propTypes = {
  theme: PropTypes.string.isRequired,
  position: PropTypes.string,
  expandWidth: PropTypes.string,
  children: PropTypes.shape({}).isRequired,
  onToggle: PropTypes.func.isRequired,
};

SideDock.defaultProps = {
  position: SideDockPosition.LEFT,
  expandWidth: "24rem",
};

export default mapThemeToProps(SideDock);
