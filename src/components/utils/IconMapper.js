const IconMapper = {
  age: "id-number",
  adress: "map-marker",
  country: "globe",
  work: "briefcase",
  workLocation: "office",
};
export default IconMapper;
