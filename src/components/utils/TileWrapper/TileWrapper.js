import React from "react";
import PropTypes from "prop-types";

import { Icon } from "@blueprintjs/core";

import ViewSelector from "./ViewSelector";

import cssClasses from "./TileWrapper.module.scss";
import { mapThemeToProps } from "../../../store/theme";

function TileWrapper(props) {
  const {
    children,
    heading,
    headingIcon,
    height = 1,
    width = 1,
    viewOptions,
    theme,
    onViewChange,
  } = props;

  const onViewSelect = view => {
    if (onViewChange) {
      onViewChange(view.value);
    }
  };

  const cellInfo = {
    gridColumnEnd: `span ${width}`,
    gridRowEnd: `span ${height}`,
  };

  return (
    <div className={cssClasses.root} data-theme={theme} style={cellInfo}>
      <header className={cssClasses.header}>
        {heading && (
          <div className={cssClasses.headerContent}>
            <h3 className={cssClasses.heading}>
              {headingIcon && <Icon className={cssClasses.headingIcon} icon={headingIcon} />}{" "}
              {heading}
            </h3>
          </div>
        )}
        {viewOptions && (
          <div className={cssClasses.headerContent}>
            <ViewSelector viewOptions={viewOptions} onSelect={onViewSelect} />
          </div>
        )}
      </header>
      {children}
    </div>
  );
}

TileWrapper.propTypes = {
  heading: PropTypes.string,
  headingIcon: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  viewOptions: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    })
  ),
  onViewChange: PropTypes.func,
};

TileWrapper.defaultProps = {
  heading: null,
  headingIcon: null,
  width: 1,
  height: 1,
  viewOptions: null,
  onViewChange: null,
};

export default mapThemeToProps(TileWrapper);
