import React, { useState } from "react";
import PropTypes from "prop-types";

import { Alignment, Button, Menu, MenuItem, Popover, Position } from "@blueprintjs/core";
import { IconNames } from "@blueprintjs/icons";

import cssClasses from "./ViewSelector.module.css";

function ViewSelector({ viewOptions, onSelect }) {
  const [selectedIndex, setSelectedIndex] = useState(0);

  const selectItem = index => {
    const item = viewOptions[index];
    if (onSelect) onSelect(item);
    setSelectedIndex(index);
  };

  const renderViewSelectorMenu = () => {
    return (
      <Menu>
        {viewOptions.map((item, index) => (
          <MenuItem
            key={item.text}
            text={item.text}
            onClick={() => selectItem(index)}
            disabled={index === selectedIndex}
          />
        ))}
      </Menu>
    );
  };

  const renderViewSelectorButton = () => {
    return <Button rightIcon={IconNames.CARET_DOWN} alignText={Alignment.LEFT} minimal />;
  };

  return (
    <div className={cssClasses.root}>
      {viewOptions.length > 1 && (
        <Popover content={renderViewSelectorMenu()} position={Position.BOTTOM_RIGHT}>
          {renderViewSelectorButton()}
        </Popover>
      )}
    </div>
  );
}

ViewSelector.propTypes = {
  viewOptions: PropTypes.arrayOf(
    PropTypes.shape({
      text: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    })
  ).isRequired,
  onSelect: PropTypes.func.isRequired,
};

export default ViewSelector;
