import React, { useState } from "react";
// import { connect as connectRedux } from "react-redux";
import { InputGroup, Button } from "@blueprintjs/core";
import axios from "axios";
import cssModule from "./Login.module.scss";

// import { login } from "../../store/auth/authActions";

import { mapThemeToProps } from "../../store/theme";

// import logo from "../../logo.svg";
const logo = "sdfm";

const getResearcher = accessToken => {
  axios
    .get("/cases/1234/researcher", {
      baseURL: "http://localhost:9001",
      headers: {
        Authorization: `Bearer ${accessToken}`,
        Accept: "application/json",
      },
    })
    .then(response => {
      console.log("Researcher: ", response);
    })
    .catch(error => {
      console.log("Researcher - Error: ", error);
    });
};

const Login = () => {
  const [username, setUsername] = useState("username");
  const [password, setPassword] = useState("password");
  const [showPassword, setShowPassword] = useState(false);
  const [errorText, setErrorText] = useState("");

  // const altText = props.appName + " logo";
  const lucinity = "LUCINITY";
  const clearlens = "ClearLens";
  const altText = "Lucinity ClearLens";
  const appLogo = <img src={logo} className={cssModule.Logo} alt={altText} />;
  const lucinityName = <span className={cssModule.LucinityName}>{lucinity}</span>;
  const appName = <span className={cssModule.AppName}>{clearlens}</span>;

  const loginError = error => {
    console.debug("In Login page - got ERROR");
    if (error && error.response && error.response.status === 401) {
      setErrorText("Incorrect username or password!");
    }
  };

  const login = () => {
    /* ignore */
  };

  const lockButton = (
    <Button
      icon={showPassword ? "eye-open" : "eye-off"}
      minimal
      onClick={() => setShowPassword(!showPassword)}
    />
  );

  return (
    <div className={cssModule.root}>
      <div className={cssModule.contentArea}>
        <div className={cssModule.logoArea}>
          {appLogo}
          {lucinityName}
          {appName}
        </div>
        <InputGroup
          type="text"
          name="username"
          value={username}
          onChange={e => setUsername(e.target.value)}
        />
        <InputGroup
          type={showPassword ? "text" : "password"}
          name="password"
          value={password}
          rightElement={lockButton}
          onChange={e => setPassword(e.target.value)}
        />

        <Button onClick={() => login({ username, password }, loginError)}>Login</Button>

        <p className={cssModule.error}>{errorText}</p>
      </div>
      <div>
        <button type="button" onClick={() => getResearcher(username)}>
          Researcher
        </button>
      </div>
    </div>
  );
};
/*
const mapDispatchToProps = {
  login,
};

export default connectRedux(null, mapDispatchToProps)(mapThemeToProps(Login)); */

export default mapThemeToProps(Login);
