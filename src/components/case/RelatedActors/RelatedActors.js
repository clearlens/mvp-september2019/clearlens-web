import React, { useState } from "react";
import { IconNames } from "@blueprintjs/icons";

import { TileWrapper } from "../../utils";
import cssClasses from "./RelatedActors.module.scss";

import Actor from "./Actor";

function RelatedActors(props) {
  const [actors] = useState([
    {
      name: "David Butler (Focal)",
      active: true,
      info: {
        age: 76,
        adress: "New Road 75, Tampa, Florida",
        country: "Iceland",
        facebook: null,
        twitter: null,
        instagram: null,
        work: "Retired",
        workLocation: "No work location",
      },
    },
    {
      name: "Tayler Joiner (Beneficiary - PEP)",
      active: false,
      info: {
        age: 76,
        adress: "New Road 75, Tampa, Florida",
        country: "Iceland",
        facebook: null,
        twitter: null,
        instagram: null,
        work: "Retired",
        workLocation: "No work location",
      },
    },
    {
      name: "Christian Russell (Counterpart - Low Risk)",
      active: false,
      info: {
        age: 76,
        adress: "New Road 75, Tampa, Florida",
        country: "Iceland",
        facebook: null,
        twitter: null,
        instagram: null,
        work: "Retired",
        workLocation: "No work location",
      },
    },
    {
      name: "Onyama Limba (Coutnerpart - Low Risk)",
      active: false,
      info: {
        age: 76,
        adress: "New Road 75, Tampa, Florida",
        country: "Iceland",
        facebook: null,
        twitter: null,
        instagram: null,
        work: "Retired",
        workLocation: "No work location",
      },
    },
  ]);

  return (
    <TileWrapper
      heading="RelatedActors"
      headingIcon={IconNames.EYE_OPEN}
      width={2}
      height={2}
      viewOptions={[
        {
          text: "List by Risk",
          value: "RISK",
        },
        {
          text: "List by Age",
          value: "AGE",
        },
      ]}>
      <div className={cssClasses.root}>
        {actors.map(actor => (
          <Actor key={`actor${actor.name.replace(" ", "")}`} actor={actor} theme={props.theme} />
        ))}
      </div>
    </TileWrapper>
  );
}

export default RelatedActors;
