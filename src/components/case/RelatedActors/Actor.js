import React, { useState } from "react";
import classNames from "classnames";
import { Icon } from "@blueprintjs/core";

import { IconMapper } from "../../utils";

import cssClasses from "./Actor.module.scss";
import { mapThemeToProps } from "../../../store/theme";

function Actor({ actor, theme }) {
  const [isActive, setActive] = useState(actor.active);

  return (
    <div className={classNames(cssClasses.root, cssClasses[`root--${theme.name}`])}>
      <Icon
        className={classNames(cssClasses.personIcon, cssClasses[`personIcon--${theme.name}`])}
        icon="person"
      />
      <div
        aria-hidden
        className={classNames(cssClasses.accordion)}
        onClick={() => setActive(!isActive)}>
        {actor.name}
      </div>
      <div
        className={classNames(cssClasses.panel, isActive ? cssClasses.active : cssClasses.hidden)}>
        {Object.keys(actor.info).map(key => {
          if (actor.info[key] !== null) {
            return (
              <div key={key} className={cssClasses.infoWrapper}>
                <Icon icon={IconMapper[key]} className={cssClasses.infoIcon} />
                <p>{actor.info[key]}</p>
              </div>
            );
          }
          return "";
        })}
      </div>
    </div>
  );
}

export default mapThemeToProps(Actor);
