import React, { Component } from "react";
import PropTypes from "prop-types";
import cn from "classnames";
import { DropTarget } from "react-dnd";

import CaseCard from "./CaseCard";
import cssClasses from "./CaseStack.module.scss";

class CaseStack extends Component {
  cardClicked = caseData => {
    const { id, onCardClick } = this.props;
    onCardClick(id, caseData);
  };

  cardDoubleClicked = caseData => {
    const { id, onCardDoubleClick } = this.props;
    onCardDoubleClick(id, caseData);
  };

  cardDropped = caseData => {
    const { id, onCardDrop } = this.props;
    onCardDrop(id, caseData);
  };

  render() {
    const { id, cases, connectDropTarget, hovered, theme } = this.props;
    return connectDropTarget(
      <div
        id={`card-stack-${id}`}
        className={cn(cssClasses.root, { [cssClasses[`${theme}--hovered`]]: hovered })}>
        {cases.map(caseData => (
          <CaseCard
            caseData={caseData}
            onClick={this.cardClicked}
            onDoubleClick={this.cardDoubleClicked}
            onDrop={this.cardDropped}
            key={`card::${caseData.id}`}
          />
        ))}
      </div>
    );
  }
}

CaseStack.propTypes = {
  id: PropTypes.string.isRequired,
  cases: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      description: PropTypes.string.isRequired,
    })
  ).isRequired,
  onCardDrop: PropTypes.func.isRequired,
  onCardClick: PropTypes.func.isRequired,
  onCardDoubleClick: PropTypes.func.isRequired,
};

CaseStack.mapStateToProps = state => {
  return {
    theme: state.theme,
  };
};

export default DropTarget("case-card", {}, (connect, monitor) => {
  return {
    connectDropTarget: connect.dropTarget(),
    hovered: monitor.isOver(),
    item: monitor.getItem(),
  };
})(CaseStack);
