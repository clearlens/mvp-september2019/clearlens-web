import React, { Component } from "react";
import PropTypes from "prop-types";
import cn from "classnames";

import { DragSource } from "react-dnd";
import cssModule from "./CaseCard.module.scss";

class CaseCard extends Component {
  cardClicked = () => {
    const { caseData, onClick } = this.props;
    onClick.apply(this, [caseData]);
  };

  cardDoubleClick = () => {
    const { caseData, onDoubleClick } = this.props;
    onDoubleClick.apply(this, [caseData]);
  };

  render() {
    const { caseData, isDragging, connectDragSource, theme } = this.props;

    return connectDragSource(
      <button
        type="button"
        className={cn(cssModule.root, cssModule[`root--${theme}`])}
        onClick={this.cardClicked}
        onDoubleClick={this.cardDoubleClick}
        style={{ opacity: isDragging ? "0.2" : "1" }}>
        <div>{caseData.id}</div>
        <div>{caseData.description}</div>
      </button>
    );
  }
}

CaseCard.propTypes = {
  caseData: PropTypes.shape({
    id: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
  }).isRequired,
  onClick: PropTypes.func.isRequired,
  onDoubleClick: PropTypes.func.isRequired,
  // onDrop: PropTypes.func.isRequired,
};

CaseCard.mapStateToProps = state => {
  return {
    theme: state.user.theme,
  };
};

export default DragSource(
  "case-card",
  {
    beginDrag(props) {
      return props.caseData;
    },
    endDrag(props, monitor) {
      if (monitor.didDrop()) {
        // ignore this
      }
      props.onDrop(props.caseData);
    },
  },
  (connect, monitor) => {
    return {
      connectDragSource: connect.dragSource(),
      connectDragPreview: connect.dragPreview(),
      isDragging: monitor.isDragging(),
    };
  }
)(CaseCard);
