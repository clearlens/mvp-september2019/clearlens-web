import React from "react";

import HTML5Backend from "react-dnd-html5-backend";
import { DragDropContext } from "react-dnd";

import { useCaseContext } from "../../../contexts/CaseContext";

import cssClasses from "./CaseBoard.module.scss";
import CaseStack from "./CaseStack";
import { mapThemeToProps } from "../../../store/theme";

const notStarted = [
  { id: "C-1234-5678", description: "AML Funneling" },
  { id: "C-1234-5679", description: "AML Funneling" },
  { id: "C-1234-5680", description: "AML Funneling" },
  { id: "C-1234-5681", description: "AML Funneling" },
  { id: "C-1234-5682", description: "AML Funneling" },
  { id: "C-1234-5683", description: "AML Funneling" },
  { id: "C-1234-5684", description: "AML Funneling" },
  { id: "C-1234-5685", description: "AML Funneling" },
];

const inAssessment = [
  { id: "C-1234-5686", description: "AML Funneling" },
  { id: "C-1234-5687", description: "AML Funneling" },
  { id: "C-1234-5688", description: "AML Funneling" },
  { id: "C-1234-5689", description: "AML Funneling" },
  { id: "C-1234-5690", description: "AML Funneling" },
];

const inInvestigation = [
  { id: "C-1234-5691", description: "AML Funneling" },
  { id: "C-1234-5692", description: "AML Funneling" },
  { id: "C-1234-5693", description: "AML Funneling" },
  { id: "C-1234-5694", description: "AML Funneling" },
];

const inEscalation = [{ id: "C-1234-5695", description: "AML Funneling" }];

function CaseBoard(props) {
  const { history, theme } = props;
  const { dispatch } = useCaseContext();

  const openCase = caseId => {
    dispatch({
      type: "openCase",
      caseId,
    });

    history.push(`/case/${caseId}`);
  };

  const cardClicked = (stackId, caseData) => {
    // eslint-disable-next-line no-console
    console.log(`Card ${caseData.id} was clicked from ${stackId}`, caseData);
  };

  const cardDoubleClicked = (stackId, caseData) => {
    // eslint-disable-next-line no-console
    console.log(`Card ${caseData.id} was double clicked from ${stackId}`, caseData);
    openCase(caseData.id);
  };

  const cardDropped = (stackId, caseData) => {
    // eslint-disable-next-line no-console
    console.log(`Card ${caseData.id} was dropped from ${stackId}`, caseData);
  };

  return (
    <div className={cssClasses.root} data-theme={theme}>
      <div className={cssClasses.swimlane}>
        <div className={cssClasses.swimlaneHeader}>Not Started</div>
        <CaseStack
          id="not-started"
          cases={notStarted}
          onCardClick={cardClicked}
          onCardDoubleClick={cardDoubleClicked}
          onCardDrop={cardDropped}
          theme={theme}
        />
      </div>
      <div className={cssClasses.swimlane}>
        <div className={cssClasses.swimlaneHeader}>In Assessment</div>
        <CaseStack
          id="in-assessment"
          cases={inAssessment}
          onCardClick={cardClicked}
          onCardDoubleClick={cardDoubleClicked}
          onCardDrop={cardDropped}
          theme={theme}
        />
      </div>
      <div className={cssClasses.swimlane}>
        <div className={cssClasses.swimlaneHeader}>In investigation</div>
        <CaseStack
          id="in-investigation"
          cases={inInvestigation}
          onCardClick={cardClicked}
          onCardDoubleClick={cardDoubleClicked}
          onCardDrop={cardDropped}
          theme={theme}
        />
      </div>
      <div className={cssClasses.swimlane}>
        <div className={cssClasses.swimlaneHeader}>In Escalation</div>
        <CaseStack
          id="in-escalation"
          cases={inEscalation}
          onCardClick={cardClicked}
          onCardDoubleClick={cardDoubleClicked}
          onCardDrop={cardDropped}
          theme={theme}
        />
      </div>
    </div>
  );
}

const DndCaseBoard = DragDropContext(HTML5Backend)(CaseBoard);

export default mapThemeToProps(DndCaseBoard);
