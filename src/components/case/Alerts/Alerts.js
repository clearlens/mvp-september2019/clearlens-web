import React, { useState } from "react";
import { HTMLTable } from "@blueprintjs/core";

import { TileWrapper } from "../../utils";
import cssClasses from "./Alerts.module.scss";

function Alerts() {
  const [alerts, setAlerts] = useState([
    {
      alertId: "A-4544-1122",
      system: "ClearLens",
      behaviour: "Exessive transaction",
      activity: "Deposit",
      severity: "0",
    },
    {
      alertId: "A-4544-3322",
      system: "Actimize",
      behaviour: "Exessive transaction",
      activity: "Deposit",
      severity: "2",
    },
    {
      alertId: "A-4544-1124",
      system: "Mantas",
      behaviour: "Exessive transaction",
      activity: "Deposit",
      severity: "1",
    },
    {
      alertId: "A-4544-3453",
      system: "ClearLens",
      behaviour: "Exessive transaction",
      activity: "Deposit",
      severity: "2",
    },
    {
      alertId: "A-4544-6674",
      system: "ClearLens",
      behaviour: "Exessive transaction",
      activity: "Withdrawal",
      severity: "0",
    },
  ]);

  const changeOrderBy = field => {
    const orderResult = [...alerts];

    switch (field) {
      case "ALERT":
        orderResult.sort((a, b) => {
          if (a.alertId < b.alertId) {
            return -1;
          }
          if (a.alertId > b.alertId) {
            return 1;
          }
          return 0;
        });
        break;

      case "SEVERITY":
        orderResult.sort((a, b) => a.severity - b.severity);
        break;

      case "SYSTEM":
        orderResult.sort((a, b) => {
          if (a.system < b.system) {
            return -1;
          }
          if (a.system > b.system) {
            return 1;
          }
          return 0;
        });
        break;

      default:
        break;
    }

    setAlerts(orderResult);
  };

  return (
    <TileWrapper
      heading="Alerts"
      width={4}
      height={1}
      viewOptions={[
        { text: "By Alert", value: "ALERT" },
        { text: "By Severity", value: "SEVERITY" },
        { text: "By System", value: "SYSTEM" },
      ]}
      onViewChange={changeOrderBy}>
      <HTMLTable className={cssClasses.alertsTable} condensed striped>
        <thead>
          <tr>
            <th>Alert</th>
            <th>System</th>
            <th>Behaviour</th>
            <th>Activity</th>
          </tr>
        </thead>
        <tbody>
          {alerts.map(alert => (
            <tr key={`alerts::alert-table-item::${alert.alertId}`}>
              <td>{alert.alertId}</td>
              <td>{alert.system}</td>
              <td>{alert.behaviour}</td>
              <td>{alert.activity}</td>
            </tr>
          ))}
        </tbody>
      </HTMLTable>
    </TileWrapper>
  );
}

export default Alerts;
