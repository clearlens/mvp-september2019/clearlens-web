import React, { useState } from "react";
import { HTMLTable } from "@blueprintjs/core";

import { TileWrapper } from "../../utils";
import cssClasses from "./Narrative.module.scss";

function Narrative() {
  const [view, setView] = useState("TEXTUAL");

  const heading = view === "TABULAR" ? "Tabular View" : "Consern";

  return (
    <TileWrapper
      heading={heading}
      width={4}
      height={1}
      viewOptions={[{ text: "Textual", value: "TEXTUAL" }, { text: "Tabular", value: "TABULAR" }]}
      onViewChange={setView}>
      {view === "TEXTUAL" && (
        <div>
          The alerted activity occurred on 03/01/2019 to 04/21/2019 and totalled US$12,045,081.99.
          The activity consisted of 1780 transactions from the focused account to the beneficiary
          David Butler, a 76-year-old male from Tampa, Florida and then out again to beneficiary
          Tayler Joyner (PEP) at Bank B. The transaction activity appears suspicious as David Butler
          ́s median income for the past 10 years has been US$127,888, the median transaction volume
          before this activity was US$15,235 a month, as well as the median transaction volume, was
          21 transaction per month.
        </div>
      )}
      {view === "TABULAR" && (
        <HTMLTable className={cssClasses.tableView} condensed striped>
          <thead>
            <tr>
              <th>Project</th>
              <th>Description</th>
              <th>Technologies</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Blueprint</td>
              <td>CSS framework and UI toolkit</td>
              <td>Sass, TypeScript, React</td>
            </tr>
            <tr>
              <td>TSLint</td>
              <td>Static analysis linter for TypeScript</td>
              <td>TypeScript</td>
            </tr>
            <tr>
              <td>Plottable</td>
              <td>Composable charting library built on top of D3</td>
              <td>SVG, TypeScript, D3</td>
            </tr>
          </tbody>
        </HTMLTable>
      )}
    </TileWrapper>
  );
}

export default Narrative;
