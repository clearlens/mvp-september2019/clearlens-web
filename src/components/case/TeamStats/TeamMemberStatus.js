import React from "react";
import PropTypes from "prop-types";

import Classes from "./TeamMemberStatus.module.scss";
import defaultAvatar from "./default-avatar.png";

function TeamMemberStatus({ member }) {
  const { firstName, lastName, avatar = defaultAvatar, cases } = member;
  const { assigned, completed } = cases;
  const progressStatus = completed > 0 && assigned > 0 ? (completed / assigned) * 100 : 0;

  return (
    <div className={Classes.root}>
      <div className={Classes.avatar}>
        <img
          src={avatar}
          className={Classes.avatar}
          title={`${firstName}'s profile image`}
          alt={`${firstName} ${lastName}`}
        />
      </div>
      <div className={Classes.status}>
        <div className={Classes.statusHeader}>
          <div className={Classes.name}>{`${firstName} ${lastName}`}</div>
          <div className={Classes.numOfCases}>
            {completed} of {assigned} Cases
          </div>
        </div>
        <div className={Classes.progressBar}>
          <div className={Classes.progressStatus} style={{ width: `${progressStatus}%` }} />
        </div>
      </div>
    </div>
  );
}

TeamMemberStatus.propTypes = {
  member: PropTypes.shape({
    firstName: PropTypes.string.isRequired,
    lastName: PropTypes.string.isRequired,
    avatar: PropTypes.string,
    cases: PropTypes.shape({
      assigned: PropTypes.number.isRequired,
      completed: PropTypes.number.isRequired,
    }),
  }).isRequired,
};

export default TeamMemberStatus;
