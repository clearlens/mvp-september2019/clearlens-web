import React from "react";
import cn from "classnames";
import { Alignment, Button, Menu, MenuItem, Popover, Position } from "@blueprintjs/core";
import TeamMemberStatus from "./TeamMemberStatus";

import Classes from "./TeamStats.module.scss";

const teamMembers = [
  {
    firstName: "Daniel",
    lastName: "Mathis",
    cases: {
      assigned: 39,
      completed: 30,
    },
  },
  {
    firstName: "Jenifer",
    lastName: "Saxton",
    cases: {
      assigned: 35,
      completed: 32,
    },
  },
  {
    firstName: "Michael",
    lastName: "Dubois",
    cases: {
      assigned: 20,
      completed: 18,
    },
  },
  {
    firstName: "Amy",
    lastName: "Connolly",
    cases: {
      assigned: 45,
      completed: 30,
    },
  },
];

function TeamStats() {
  return (
    <div className={Classes.root}>
      <div className={Classes.header}>
        <h3 className={cn(Classes.heading, Classes.headerContent)}>Team&apos;s Stats</h3>
        <div className={cn(Classes.periodSelector, Classes.headerContent)}>
          <Popover
            content={
              <Menu>
                <MenuItem text="Today" />
                <MenuItem text="This Week" />
                <MenuItem text="This Month" />
              </Menu>
            }
            position={Position.BOTTOM_RIGHT}>
            <Button rightIcon="caret-down" alignText={Alignment.LEFT} text="Today" minimal />
          </Popover>
        </div>
      </div>
      <div className={Classes.teamMembers}>
        {teamMembers.map(member => (
          <TeamMemberStatus
            key={`team-member::${member.firstName} ${member.lastName}`}
            member={member}
          />
        ))}
      </div>
    </div>
  );
}

export default TeamStats;
