import axios from "axios";

/**
 * Authenticator, both SSO and direct Open-ID.
 * Calls standard Open-ID protocol endpoints.
 */
class Authenticator {
  constructor(config) {
    if (config) {
      this.authServerURL = config["auth-server-url"];
      this.realm = config.realm;
      this.clientId = config.resource;
    } else {
      this.authServerURL = "http://localhost:8080/auth";
      this.realm = "mybank";
      this.clientId = "cm-web";
    }
  }

  // TODO : This client does not conform to OAuth 2.0 / OpenID specs.
  // MISSING: Validate tokens, see 3.1.3.7 @ https://openid.net/specs/openid-connect-core-1_0.html#TokenEndpoint
  /*
{
    "access_token": "eyJhskljdflksjdfklds",
    "expires_in": 300,
    "refresh_expires_in": 1800,
    "refresh_token": "eyJhbGciOiJIUzI1NiIsInR5c",
    "token_type": "bearer",
    "id_token": "eyJhbGciOiJSUzI1NiIsInR5cCIgOi",
    "not-before-policy": 1557241277,
    "session_state": "60904fb5-07ca-44b4-9d4d-d2a5fcc9ae4c",
    "scope": "openid email address profile"
}
*/

  /*
How to delete the cookies:

const deleteCookie = (cname) => {
  let d = new Date(); //Create an date object
  d.setTime(d.getTime() - (1000*60*60*24)); //Set the time to the past. 1000 milliseonds = 1 second
  let expires = "expires=" + d.toGMTString(); //Compose the expirartion date
  document.cookie = cname+"="+"; "+expires;//Set the cookie with name and the expiration date
}

// in methods:
    if (!state.useSSO) {
      console.log("Even though logout failed we are deleting cookies and resetting auth");

      // Deleting cookies
      deleteCookie("AUTH_SESSION_ID");
      deleteCookie("KEYCLOAK_IDENTITY");
    }

 */

  /**
   * Takes in login information so auth server can authenticate the user.
   * loginInfo contains email and password fields.
   *
   * Returns a promise that includes the response from the auth server.
   * @param loginInfo
   */
  login(loginInfo) {
    const scope = "openid+email+profile+address";
    const url = `/realms/${this.realm}/protocol/openid-connect/token`;

    if (!loginInfo) {
      return Promise.reject(new Error("Missing loginInfo"));
    }

    // Parse the email
    const username = loginInfo.username ? loginInfo.username.split("@")[0] : loginInfo.username;

    const data = `${"grant_type=password&client_id="}${
      this.clientId
    }&scope=${scope}&username=${username}&password=${loginInfo.password}`;

    return axios.post(url, data, {
      baseURL: this.authServerURL,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    });
  }

  logout(refreshToken) {
    if (!refreshToken) {
      return Promise.reject(new Error("Missing refresh token"));
    }

    const url = `/realms/${this.realm}/protocol/openid-connect/logout`;

    const data = `client_id=${this.clientId}&refresh_token=${refreshToken}`;

    return axios.post(url, data, {
      baseURL: this.authServerURL,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    });
  }

  refreshToken(refreshToken) {
    if (!refreshToken) {
      return Promise.reject(new Error("Missing refresh token"));
    }

    const url = `/realms/${this.realm}/protocol/openid-connect/token`;

    const data = `${"grant_type=refresh_token&client_id="}${
      this.clientId
    }&refresh_token=${refreshToken}`;

    return axios.post(url, data, {
      baseURL: this.authServerURL,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
    });
  }
}

export default Authenticator;
