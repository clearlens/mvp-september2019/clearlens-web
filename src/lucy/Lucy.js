import React, { useState } from "react";
import { Radio, RadioGroup } from "@blueprintjs/core";

import cssClasses from "./Lucy.module.scss";
import { SideDock, SideDockPosition } from "../components/utils";
import { mapThemeToProps } from "../store/theme";

function Lucy({ scope, theme }) {
  const [isExpanded, setIsExpanded] = useState(false);

  const toggle = expanded => {
    setIsExpanded(expanded);
  };

  return (
    <SideDock position={SideDockPosition.RIGHT} expandWidth="40rem" onToggle={toggle}>
      <div className={cssClasses.root} data-expanded={isExpanded} data-theme={theme}>
        <div className={cssClasses.headingBar}>
          <span className={cssClasses.headingText}>Lucy</span>
        </div>
        <div className={cssClasses.mainContent}>
          {scope === "case-review" && (
            <>
              <h4>Normal transactions</h4>
              <p>Would the beneficiary receive transaction from this many types of orginiators</p>
              <ul>
                <RadioGroup onChange={() => {}}>
                  <Radio key="review-assistant::radio::1" label="Yes" value="Yes" />
                  <Radio key="review-assistant::radio::2" label="No" value="No" />
                </RadioGroup>
              </ul>
            </>
          )}
          {scope === "case-research" && (
            <>
              <h4>Stuff to search for</h4>
              <p>You should search for the following issues</p>
              <ul>
                <RadioGroup onChange={() => {}}>
                  <Radio key="research-assistant::radio::1" label="Yes" value="Yes" />
                  <Radio key="research-assistant::radio::2" label="No" value="No" />
                </RadioGroup>
              </ul>
            </>
          )}
        </div>
      </div>
    </SideDock>
  );
}

export default mapThemeToProps(Lucy);
