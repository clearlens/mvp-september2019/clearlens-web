import React, { createContext, useContext, useReducer } from "react";

const reducer = (state, action) => {
  switch (action.type) {
    case "openCase":
      if (
        state.openCases.findIndex(item => item.id === action.caseId) < 0 &&
        state.openCases.length < 3
      ) {
        return {
          ...state,
          openCases: [...state.openCases, { id: action.caseId }],
          maxOpenCasesReached: state.openCases.length >= 2,
        };
      }
      break;

    default:
      return state;
  }

  return state;
};

const initialState = {
  maxOpenCasesReached: false,
  openCases: [],
};
const CaseContext = createContext(initialState);

const useCaseContext = () => useContext(CaseContext);

function CaseContextProvider({ children }) {
  const [state, dispatch] = useReducer(reducer, initialState);

  return <CaseContext.Provider value={{ state, dispatch }}>{children}</CaseContext.Provider>;
}

export { CaseContext, CaseContextProvider, useCaseContext };
