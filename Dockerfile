FROM node:10 as builder
WORKDIR "/app"

RUN node --version
RUN npm --version

COPY package*.json ./
RUN NODE_ENV=production npm ci

COPY . .

RUN npm run test-on-ci
RUN npm run build

FROM nginx:1.15.9-alpine
COPY --from=builder /app/build /usr/share/nginx/html
EXPOSE 5000
CMD ["nginx", "-g", "daemon off;"]